package testAsso1;

//Importer les classes jdbc
import java.sql.*;
import java.util.*;

public class TestAsso1 {

	static ArrayList<Prof> listeProf = new ArrayList<Prof>();
	// La requete de test
	static final String req = "SELECT NUM_PROF, NOM_PROF, PRENOM_PROF, MAT_SPEC "
			+ "FROM PROF ";
	
	public static void main(String[] args) throws SQLException {
		// Objet materialisant la connexion a la base de donnees
		System.out.println("Connexion ");
		try (Connection conn = ConnectionUnique.getInstance().getConnection()) {
			// Connexion a la base
			System.out.println("Connecte\n");
			// Creation d'une instruction SQL
			Statement stmt = conn.createStatement();
			// Execution de la requete
			System.out.println("Execution de la requete : " + req + "\n");
			ResultSet rset = stmt.executeQuery(req);
			// Affichage du resultat
			while (rset.next()) {
				Prof P = new Prof();
				P.setNumProf(rset.getInt("NUM_PROF"));
				P.setNomProf(rset.getString("NOM_PROF"));
				P.setPrenomProf(rset.getString("PRENOM_PROF"));
				Module M = new Module(); //on cr�� notre objet module
				M.setCode(rset.getString("MAT_SPEC")); //on prend et on met le code
				P.setSpecialiste(M);
				listeProf.add(P); //sois j'affiche le tableau de prof, sois l'objet prof a chaque it�ration
				//System.out.println(E.toString() + "\n");
			}
			// Fermeture de l'instruction (liberation des ressources)
			stmt.close();
			System.out.println(listeProf.toString() ); //afichage du tableau de prof, l'affichage de prof a chaque it�rarion est plus lisible.
			System.out.println("\nOk.\n");
		} catch (SQLException e) {
			e.printStackTrace();// Arggg!!!
			System.out.println(e.getMessage() + "\n");
		}
	}
}
